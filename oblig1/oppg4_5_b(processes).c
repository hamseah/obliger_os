#include <stdio.h>     /* printf */
#include <stdlib.h>    /* exit */
#include <unistd.h>    /* fork */
#include <sys/wait.h>  /* waitpid */
#include <sys/types.h> /* pid_t */


void process(int number, int time) {
  printf("\nProsess %d kjører\n", number);
  sleep(time);
  printf("\nProsess %d kjørte i %d sekunder\n", number, time);
}


int main(void){
  pid_t p0, p1, p3;

  p0 = fork();
  if(p0==-1){
     exit(-1);
  }
  else if(p0==0){
      process(0, 1);
      p1 = fork();
      /* Sjekker at fork ikke returnerte feilmelding (-1) */
      if( p1 == -1){
	       exit(-1);
      }
      if(p1==0){
         /* P1 kjører etter P0 er ferdig */
	       process(1, 2);
      } else {
        /*  P4 som kjører samtidig som P1 */
	       process(4, 3);
      }


  } else {
      /* P2 som kjører samtidig som P0 */
      process(2, 3);
      p3 = fork();
      if(p3 == -1){
	       exit(-1);
      }
      if(p3==0){
         /* P3 som kjører etter P2 */
         process(3, 2);
      } else {
        /* p5 kjører sist, 1 sek etter p3 */
	       sleep(1);
         process(5, 3);
      }
  }

  return 0;
}
